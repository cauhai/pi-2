import React from "react";
import ReactDOM from "react-dom";
import PI2 from "./PI2.js";

// Add this plugin's DOM to the shell
(() => {
  let topElement = document.createElement('div');
  topElement.setAttribute('id', 'pi-2');
  topElement.classList.add('plugin');
  topElement.classList.add('pi-2');
  ReactDOM.render(<PI2 />, topElement);
  let shell = document.getElementById('shell');
  shell.appendChild(topElement);
})();