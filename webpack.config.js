const path = require("path");
const webpack = require("webpack");

module.exports = {
  entry: "./index.js",
  mode: "development",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: { presets: ["@babel/env"] }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      }
    ]
  },
  resolve: { extensions: ["*", ".js", ".jsx"] },
  output: {
    path: path.resolve(__dirname, "build/"),
    publicPath: "/build/",
    filename: "pi-2.js"
  },
  devServer: {
    contentBase: __dirname + '/build',
    port: 3002,
    publicPath: "http://localhost:3002/build/",
    hotOnly: true
  },
  plugins: [new webpack.HotModuleReplacementPlugin()]
};